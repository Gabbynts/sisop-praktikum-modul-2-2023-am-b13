#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

int exec(char path[], char *argv[]){
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    execvp(path, argv);
  } 
  else{
    while ((wait(&status)) > 0);
  }
}

//"-q" = quite mode, adalah command untuk menjalankan program tanpa menampilkan prosesnya pada layar
void download(char path[], char filename[]){
    char *argv[] = {"wget", path, "-O", filename, "-q", NULL};
    exec("wget", argv);
}

void unZip(char filename[]){
    char *arg[] = {"unzip", "-q", filename, NULL};
    exec("unzip", arg);
} 

void randomPicture(char path[100]){
    DIR *dp;
    struct dirent *ep;

    char animal_file[9][100];
    int count = 0;

    dp = opendir(path);

    if (dp != NULL) 
    {
      while ((ep = readdir (dp))) {
          if (strstr(ep->d_name, ".jpg") != NULL) {
              strcpy(animal_file[count], ep->d_name);
              // printf("%s %d\n", animal_file[count], count);

              count++;
          }
      }
      (void)closedir(dp);
    } else {
      perror("Couldn't open the directory");
    }

    //untuk menampilkan bilangan acak yang berbeda setiap program di jalankan
    srand(time(NULL));
    // printf("%ld \n", time(NULL));

    int random_file = rand() % count;
    // printf("%d %d %d", random_file, count, rand());
    printf("random file: %s\n\n", animal_file[random_file]);
}

void makeDirectory(char directory[]){
    char *arg[] = {"mkdir", "-p", directory, NULL};
    exec("mkdir", arg);
}

void zipDirectory(char filename[], char directory[]){
    char *arg[] = {"zip", "-rq", filename, directory, NULL};
    exec("zip", arg);
}

void moveFile(char path[100]){
    DIR *dp;
    struct dirent *ep;

    char newPath[1000];

    dp = opendir(path);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
        if (strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0) {
          continue;
        }

        if (strstr(ep->d_name, "darat") != NULL) {
          strcpy(newPath, path);
          strcat(newPath, ep->d_name);

          char *arg[] = {"mv", newPath, "HewanDarat/", NULL};
          exec("mv", arg);
        }

        if (strstr(ep->d_name, "air") != NULL) {
          strcpy(newPath, path);
          strcat(newPath, ep->d_name);

          char *arg[] = {"mv", newPath, "HewanAir/", NULL};
          exec("mv", arg);
        }

        if (strstr(ep->d_name, "amphibi") != NULL) {
            strcpy(newPath, path);
            strcat(newPath, ep->d_name);

            char *arg[] = {"mv", newPath, "HewanAmphibi/", NULL};
            exec("mv", arg);
        }
      }
      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}

int main() {
  download("https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "binatang.zip");
  
  unZip("binatang.zip");
  
  randomPicture("./");

  makeDirectory("HewanDarat");
  makeDirectory("HewanAmphibi");
  makeDirectory("HewanAir");

  moveFile("./");

  zipDirectory("HewanDarat.zip", "HewanDarat");
  zipDirectory("HewanAmphibi.zip", "HewanAmphibi");
  zipDirectory("HewanAir.zip", "HewanAir");

  // char *arg[] = {"mv", "paus_air.jpg", "HewanAir/", NULL};
  // exec("mv", arg);
}

