# sisop-praktikum-modul-2-2023-AM-B13

## Praktikum modul 2

### Kelompok B-13 dengan anggota:
| **No** | **Nama** | **NRP** | 
| ------------- | ------------- | --------- |
| 1 | Gabriella Natasya Br Ginting  | 5025211081 | 
| 2 | Muhammad Zikri Ramadhan | 5025211085 |
| 3 | Sandhika Surya Ardyanto | 5025211022 |

### Daftar Isi
- [Soal 1](#soal-1)
    - [a. Download dan Unzip](#perintah-1-a)
    - [b. Pemilihan secara acak](#perintah-1-b)
    - [c. Filter file gambar](#perintah-1-c)
    - [d. Zip direktori](#perintah-1-d)
- [Soal 2](#soal-2)
    - [a. Membuat folder](#perintah-2-a)
    - [b. Download gambar](#perintah-2-b)
    - [c. Zip folder](#perintah-2-c)
    - [d. Program Kill](#perintah-2-d)
    - [e. Membuat dua program utama](#perintah-2-e)
- [Soal 3](#soal-3)
    - [a. Unduh gambar](#perintah-3-a)
    - [b. Menghapus file](#perintah-3-b)
    - [c. Mengkategorikan pemain](#perintah-3-c)
    - [d. Membuat file txt](#perintah-3-d)
- [Soal 4](#soal-4)
    
## Soal 1
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 

```
int exec(char path[], char *argv[]){
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    execvp(path, argv);
  } 
  else{
    while ((wait(&status)) > 0);
  }
}
```

Fungsi exec diatas menerima dua parameter
- char path[] : yaitu path dari command line yang ingin di eksekusi
- char *argv[] : berisi argumen-argumen yang ingin digunakan

Fungsi exec memanggil fork() untuk membuat sebuah child process yang akan mengeksekusi command line. 

Bila ``` child_id < 0 ``` maka, program pembuatan child process gagal.

Bila ``` child_id == 0 ``` maka, proses akan dijalankan oleh child process. Dan akan mengeksekusi command line sesuai dengan parameter yang diberikan

Bila ``` child_id > 0 ``` maka, proses akan dijalankan oleh parent process. Yang dimana parent process akan menunggu child process selesai mengeksekusi dengan menggunakan wait().

- ### Perintah 1-a
Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.

```
void download(char path[], char filename[]){
    char *argv[] = {"wget", path, "-O", filename, "-q", NULL};
    exec("wget", argv);
}
```

```
download("https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "binatang.zip");
```

Fungsi download diatas menerima dua parameter
- char path[] : untuk menerima alamat file yang ingin di unduh
- char filename[] : nama file hasil unduhan

Fungsi tersebut menggunakan perintah wget untuk mengunduh file.

```
void unZip(char filename[]){
    char *arg[] = {"unzip", "-q", filename, NULL};
    exec("unzip", arg);
} 
```

```
unZip("binatang.zip");
```

Fungsi unZip diatas menerima parameter nama file yang akan di ekstrak

Fungsi tersebut menggunakan perintah unzip untuk ekstrak file.

Terdapat pula command "-q" adalah *quite mode* yang digunakan agar proses berjalannya program tidak terlihat pada terminal.

- ### Perintah 1-b
Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

```
void randomPicture(char path[100]){
    DIR *dp;
    struct dirent *ep;

    char animal_file[9][100];
    int count = 0;

    dp = opendir(path);

    if (dp != NULL) 
    {
      while ((ep = readdir (dp))) {
          if (strstr(ep->d_name, ".jpg") != NULL) {
              strcpy(animal_file[count], ep->d_name);
              // printf("%s %d\n", animal_file[count], count);

              count++;
          }
      }
      (void)closedir(dp);
    } else {
      perror("Couldn't open the directory");
    }

    //untuk menampilkan bilangan acak yang berbeda setiap program di jalankan
    srand(time(NULL));
    // printf("%ld \n", time(NULL));

    int random_file = rand() % count;
    // printf("%d %d %d", random_file, count, rand());
    printf("random file: %s\n\n", animal_file[random_file]);
}

```

```
randomPicture("./");
```

- opendir : membuka direktori
- readdir : membaca isi direktori
- closedir : menutup direktori jika loop sudah selesai
- rand() : fungsi acak
- srand() : menginisialisasi generator bilangan acak dengan waktu saat ini sebagai nilai awalnya

```
strstr(ep->d_name, ".jpg")
```

Fungsi akan memeriksa file yang dibaca apakah memiliki ekstensi ".jpg" dengan menggunakan fungsi strstr

Apabila file yang dibaca memiliki ekstensi ".jpg" maka, file akan disimpan dalam array "animal_file".

File yang sudah disimpan pada array "animal_file" akan dipilih salah satu secara acak dengan fungsi "rand" dan "srand". File yang sudah dipilih akan tercetak pada terminal.

- ### Perintah 1-c
Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

 ```
void makeDirectory(char directory[]){
    char *arg[] = {"mkdir", "-p", directory, NULL};
    exec("mkdir", arg);
}
```

```
  makeDirectory("HewanDarat");
  makeDirectory("HewanAmphibi");
  makeDirectory("HewanAir");
```
Fungsi makeDirectory dengan parameter directory yang menerima nama direktori yang ingin dibuat. Fungsi diatas juga menggunakan argumen mkdir untuk membuat direktori.

```
void moveFile(char path[100]){
    DIR *dp;
    struct dirent *ep;

    char newPath[1000];

    dp = opendir(path);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
        if (strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0) {
          continue;
        }

        if (strstr(ep->d_name, "darat") != NULL) {
          strcpy(newPath, path);
          strcat(newPath, ep->d_name);

          char *arg[] = {"mv", newPath, "HewanDarat/", NULL};
          exec("mv", arg);
        }

        if (strstr(ep->d_name, "air") != NULL) {
          strcpy(newPath, path);
          strcat(newPath, ep->d_name);

          char *arg[] = {"mv", newPath, "HewanAir/", NULL};
          exec("mv", arg);
        }

        if (strstr(ep->d_name, "amphibi") != NULL) {
            strcpy(newPath, path);
            strcat(newPath, ep->d_name);

            char *arg[] = {"mv", newPath, "HewanAmphibi/", NULL};
            exec("mv", arg);
        }
      }
      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}
```

```
 moveFile("./");
```
Fungsi diatas dengan parameter path yaitu path dari file yang ingin di pindahkan(path awal). Digunakan perintah "mv" untuk memindahkan file.

Fungsi tersebut akan mengecek apakah pada file memiliki ekstensi "darat", "air", dan "amphibi". Setiap file yang memiliki kategori masing-masing, akan dipindahkan ke direktori yang sudah dibuat sebelumnya sesuai dengan nama file dengan direkori. 

- ### Perintah 1-d
Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

```
void zipDirectory(char filename[], char directory[]){
    char *arg[] = {"zip", "-rq", filename, directory, NULL};
    exec("zip", arg);
}
```
```
  zipDirectory("HewanDarat.zip", "HewanDarat");
  zipDirectory("HewanAmphibi.zip", "HewanAmphibi");
  zipDirectory("HewanAir.zip", "HewanAir");
```

Fungsi tersebut menerima dua parameter:
- char filename[] : nama file zip yang ingin dibuat
- char directory[] : nama direktori yang ingin di zip

Fungsi zipDirectory diatas menggunakan perintah "zip" untuk mengZip direktori yang sebelumnya sudah dibuat dengan file-file yang terdapat di dalamnya.

## Soal 2
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

- ### Perintah 2-a
Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

```
  time_t epoch; 
  struct tm* dateLocal;

  epoch = time(NULL);
  dateLocal = localtime(&epoch);
```
- time_t : menyimpan waktu saat ini dalam bentuk UNIX timestamp
- localtime : mengambil waktu lokal

Pada fungsi makeDirectory, akan di set epoch yang menyimpan waktu dalam bentuk UNIX timestamp, dan ada struct tm yang mengubah bentuk UNIX oleh fungsi *localtime* untuk menjadi bentuk timestamp yang sesuai [YYYY-mm-dd_HH:mm:ss].

Setelah itu akan dilakukan looping.

```
strftime(directory, 100, "%Y-%m-%d_%H:%M:%S", dateLocal);
```

- strftime : mengonversi data waktu ke dalam bentuk string

Direktori yang dibuat akan diberi nama sesuai dengan dateLocal.
```
 if (child_id < 0) {
      exit(EXIT_FAILURE);
    }

    if (child_id == 0) {

      if (fork() == 0) {

        char *mkdir_argv[] = {"mkdir", "-p", directory, NULL};
        execvp("mkdir", mkdir_argv);

      } 
      else {
        while (wait(&status) > 0);

        download(directory);
        zip(directory);
      }
    } 
```
Pada proses looping memiliki child process yang terdapat child dan parent process juga. Pada child process tersebut akan menjalankan argumen membuat direktori dan pada parent akan memanggil fungsi download(download file) dan zip(zip & unzip direktori).

```
sleep(DIRECTORY_DELAY); 
```
- sleep : untuk menjalankan program per second

Lalu, looping akan terus dilakukan selama 30 detik sekali.

- ### Perintah 2-b
Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

```
void download(char* directory) {
  chdir(directory);

  for (int i = 0; i < 15; i++) {
    pid_t child_id;
    child_id = fork();

    if (child_id == 0) {
      time_t epoch; 
      struct tm* dateLocal;

      epoch = time(NULL);
      dateLocal = localtime(&epoch);

      char url[100];
      char filename[100];
      time_t t = time(NULL);

      sprintf(url, "https://picsum.photos/%ld", (t % 1000) + 50);
      
      strftime(filename, 100, "%Y-%m-%d_%H:%M:%S", dateLocal);

      char *argv[] = {"wget", url, "-qO", filename, NULL};
      execvp("wget", argv);
    }
    sleep(DOWNLOAD_DELAY);
  }
}
```
- chdir : mengubah *working directory* dari program ke directory yang diberikan sebagai argumen
- sprintf : melengkapi url yang ingin di download

Pada program diatas dilakukan looping sampai 15, karena diminta untuk download file sebanyak 15 per direktori. Pada program berjalan child process untuk melakukan download gambar. Filename akan diberi nama UNIX timestamp, tetapi akan diterapkan sesuai dengan localtime sama seperti penamaan dari direktori.

Lalu, untuk mendownload gambar diminta untuk mendownload dengan ukuran yang berbeda sesuai dengan ``` (t % 1000) + 50 ```. Time_t t dimanfaatkan untuk mendapatkan waktu local sebagai salah satu elemen yang digunakan untuk membentuk URL tersebut.

```
    char *argv[] = {"wget", url, "-qO", filename, NULL};
    execvp("wget", argv);
```

Dan argumen "wget" dieksekusi untuk mendownload url yang sudah disesuaikan.

- ### Perintah 2-c
Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).

```
void zip(char* directory) {

  char filename[100];
  sprintf(filename, "%.90s.zip", directory);

  char *argv[] = {"zip", "-qrm", filename, directory, NULL};
  execvp("zip", argv);
}
```

 ```
 chdir("..");
 ```

- Mengubah direktori kerja.

Pada fungsi diatas, dieksekusi argumen "zip" yang akan menzip folder yang sudah dibuat. Dan diikuti dengan argumen "-qrm", yang dimana argumen tersebut akan menghapus direktori semula yang sudah di zip sebelumnya.

- ### Perintah 2-d
Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

```
char *mode = argv[1]; 
```
Kode diatas dijalankan untuk mengambil argumen kedua dari 'argv' yang merupakan mode untuk mengeksekusi fungsi killer. Argumen ini akan menentukan sinyal yang akan digunakan pada proses yang ingin di akhiri. Apakah itu 'a' atau 'b'.

```
  fprintf(filename, "#include <stdlib.h>\n");
  fprintf(filename, "#include <string.h>\n");
  fprintf(filename, "#include <unistd.h>\n");
  fprintf(filename, "#include <stdio.h>\n");
  fprintf(filename, "#include <signal.h>\n\n");
  fprintf(filename, "int main(int argc, char *argv[]) {\n");
```
Kode diatas ditujukan untuk membuat file killer, yang dimana file tersebut akan membunuh proses PID yang telah dijalankan pada kode utama. 

```
fprintf(filename, "  pid_t pid = %d;\n", pid);
```
Digunakan untuk menyimpan PID yang akan dibunuh.

```
  char *arg[] = {"gcc", "killer.c", "-o", "killer.o", NULL};
  execvp("gcc", arg);
```
Argumen gcc dijalankan agar dapat menjalankan program dengan hanya memanggil "./killer.o"

- ### Perintah 2-e
Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

```
  if (strcmp(mode, "-a") == 0) {
    fprintf(filename, "  kill(-pid, SIGKILL);\n");
  } 

  else if (strcmp(mode, "-b") == 0) {
    fprintf(filename, "  kill(pid, SIGTERM);\n");
  }
```
apabila yang dijalankan adalah mode a, maka saat dipanggil program killer akan langsung kill program yang sedang berjalan. Namun, apabila mode b dipanggil maka killer akan mengeksekusi saat proses utama sudah selesai.

```
  fprintf(filename, "  remove(\"killer.c\");\n");
  fprintf(filename, "  remove(\"killer.o\");\n");
  fprintf(filename, "  return 0;\n");
  fprintf(filename, "}\n");

  fclose(filename);
```
Saat program killer sudah selesai dilakukan, maka file killer otomatis terhapus.


## Soal 3
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”
---
count[4] berfungsi untuk menyimpan jumlah pemain setiap posisi yang difilter yaitu kiper, bek, gelandang, dan penyerang 

`int count[4] = {0};`

---

struct player untuk memasukkan int rating yang akan digunakan untuk memfilter berdasarkan rating tertinggi dan char fl_name[100] untuk menyimpan nama file png yang akan ditulis di file txt. player[4][40], 4 sebagai jumlah posisi dan 40 sebagai asumsi jumlah pemain masing-masing posisi
```
typedef struct players_data{
    int rating;
    char fl_name[100];
} Player;

Player player[4][40]; 
```
---
- Function exec(char path[], char *argv[]) dimana path merupakan command linux seperti ls dan sort dan argv seperti {"mkdir", "mkdir", "-p", "Bek", NULL}, keduanya digunakan untuk execvp. 
- Function ini untuk mengeksekusi proses yang memiliki jeda. Dimana jika merupakan child process maka akan dieksekusi dengan execvp, jika parent process maka akan diberi jeda dengan wait.

```
void exec(char path[], char *argv[]){
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    execvp(path, argv);
  } 
  else{
    while ((wait(&status)) > 0);
  }
}
```

---

### Perintah 3-a
Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
---
- Function download(char address[], char file[]) dimana address merupakan link file zip yang didownload dan file merupakan nama file dari file zip yang didownload. 
- Function mendownload dengan command wget dan "-O" untuk nama file zip setelah didownload 

```
void download(char address[], char file[]){
    char *argv[] = {"wget", address, "-O", file, "-q", NULL};
    exec(argv[0], argv);
}
```

---
- Function unzip_delete(char file[]) dimana file merupakan nama file zip yang dihapus. 
- Pertama, file di unzip menjadi folder dengan command unzip kemudian file zip dihapus dengan command rm

``` 
void unzip_delete(char file[]){
    char *arg[] = {"unzip", "-q", file, NULL};
    exec(arg[0], arg);
    char *argv[] = {"rm", file, NULL};
    exec(argv[0], argv);
}

```
---

### Perintah 3-b
Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  
---
- Function rmv(char dir[]) dimana dir merupakan path dari direktori players yang akan menghilangkan file png yang tidak pada "ManUtd". 
- Jika merupakan file biasa dan bukan direktori dengan `ep->d_type == DT_REG` maka akan difilter file tanpa karakter "ManUtd". Jika terdapat file tersebut maka akan dihapus dengan command rm.

```
void rmv(char dir[]){
    DIR *dp;
    struct dirent *ep;
    dp = opendir(dir);
    char *token;
    char file[100];
    if(dir == NULL){
        printf("Failed to open directory");
        return;
    }
    while((ep = readdir(dp)) != NULL) {
        if(ep->d_type == DT_REG){
            char *token = ep->d_name;
            char *separator = strchr(token, '_');
            if(separator != NULL){
                if(strstr(token, "ManUtd")){
                    continue;
                }
                else{
                    sprintf(file, "%s%s", dir, token);
                    char *arg[] = {"rm", file, NULL};
                    exec(arg[0], arg);
                }
            }
        }
    }    
}
```
---

### Perintah 3-c
Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
---
- Function makeDirectory(char path[]) dimana path merupakan path pada direktori players. 
- Function ini untuk membuat direktori dengan commanf mkdir untuk masing-masing posisi.
- Berjalan bersamaan dengan fork pid hingga pembuatan direktori selesai baru menggunakan wait hingga semua proses selesai.

``` 
void makeDirectory(char path[]){   
    pid_t pid;
    int status;
    for(int i=0; i<4; i++){
        pid = fork();
        char dir[100];
        if(pid < 0){
            exit(EXIT_FAILURE);
        }
        else if(pid == 0){
            if(i == 0){
            	sprintf(dir, "%sKiper", path);
                char *arga[] = {"mkdir", "-p", dir, NULL};
                execvp(arga[0], arga);
            }
            else if(i == 1){
                sprintf(dir, "%sBek", path);
                char *argb[] = {"mkdir", "-p", dir, NULL};
                execvp(argb[0], argb);
            }
            else if(i == 2){
                sprintf(dir, "%sGelandang", path);
                char *argc[] = {"mkdir", "-p", dir, NULL};
                execvp(argc[0], argc);
            }
            else if(i == 3){
                sprintf(dir, "%sPenyerang", path);
                char *argd[] = {"mkdir", "-p", dir, NULL};
                execvp(argd[0], argd);
            }
        }
    }
    while ((pid = wait(&status)) > 0);
}

```
---
- Function filter(char path[]) dimana path merupakan path pada direktori players. 
- Command mv untuk memindahkan file png ke direktori masing-masing sesuai posisi sekaligus menyimpan data rating dan nama file ke struct player.
- Menggunakan strtok untuk mendapatkan nilai rating kemudian diubah menjadi tipe int dengan atoi.
- Berjalan bersamaan dengan fork pid hingga pembuatan direktori selesai baru menggunakan wait hingga semua proses selesai. 

``` 
void filter(char path[]){
    DIR *dp;
    struct dirent *ep;    
    dp = opendir(path);
    if (dp != NULL){
        pid_t child_id;
        int status;
        while(ep = readdir(dp)){   
            char source[100];
            char target[100];
            char *filename = ep->d_name;
            char *token;    
            Player data_p;        
            if (ep->d_type == DT_REG){
                child_id = fork();
                if(child_id < 0){
                	exit(EXIT_FAILURE);
            	}	
            	else if(child_id == 0){                
                    if(strstr(filename, "Bek") != NULL) {            
                        sprintf(source, "%s%s", path, filename);
                	    sprintf(target, "%sBek/", path); 
                        char *arg[] = {"mv", source, target, NULL};
                        execvp(arg[0], arg);
                        strcpy(data_p.fl_name, filename);
                        token = strtok(filename, "_");
                        token = strtok(NULL, "_");
                        token = strtok(NULL, "_");
                        token = strtok(NULL, "_");
                        data_p.rating = atoi(token);
                        player[0][count[0]] = data_p;
                        count[0]++;
                    }
                    else if(strstr(filename, "Gelandang") != NULL) {
                        sprintf(source, "%s%s", path, filename);
                	    sprintf(target, "%sGelandang/", path); 
                        char *arg[] = {"mv", source, target, NULL};
                        execvp(arg[0], arg);
                        strcpy(data_p.fl_name, filename);
                        token = strtok(filename, "_");
                        token = strtok(NULL, "_");
                        token = strtok(NULL, "_");
                        token = strtok(NULL, "_");
                        data_p.rating = atoi(token);
                        player[1][count[1]] = data_p;
                        count[1]++;
                    }
                    else if(strstr(filename, "Penyerang") != NULL) {
                        sprintf(source, "%s%s", path, filename);
                	    sprintf(target, "%sPenyerang/", path); 
                        char *arg[] = {"mv", source, target, NULL};
                        execvp(arg[0], arg);
                        strcpy(data_p.fl_name, filename);
                        token = strtok(filename, "_");
                        token = strtok(NULL, "_");
                        token = strtok(NULL, "_");
                        token = strtok(NULL, "_");
                        data_p.rating = atoi(token);
                        player[2][count[2]] = data_p;
                        count[2]++;
                    }
                    else if(strstr(filename, "Kiper") != NULL) {     
                        sprintf(source, "%s%s", path, filename);
                	    sprintf(target, "%sKiper/", path); 
                        char *arg[] = {"mv", source, target, NULL};
                        execvp(arg[0], arg);
                        strcpy(data_p.fl_name, filename);
                        token = strtok(filename, "_");
                        token = strtok(NULL, "_");
                        token = strtok(NULL, "_");
                        token = strtok(NULL, "_");
                        data_p.rating = atoi(token);
                        player[3][count[3]] = data_p;
                        count[3]++;
                    }
                }
            }
        }                   
            (void) closedir (dp);
            while ((child_id = wait(&status)) > 0);
    }
    else{
        perror ("Couldn't open the directory");
    }
}
```
---

### Perintah 3-d
Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/
---
- Function sort() berfungsi untuk mengurutkan pemain berdasarkan rating tertinggi
- i untuk menandai posisi setiap pemain sebanyak 4
- j untuk jumlah pemain dalam direktori masing-masing posisi dengan count[i]
- k untuk mengurutkan ke belakang apakah ada yang lebih kecil sebelumnya. Jika ada maka akan ditukar
```
Player temp = player[i][k-1];
player[i][k-1] = player[i][k];
player[i][k] = temp; 
```

```
void sort(){
    for(int i=0; i<4; i++){
        for(int j=1; j<count[i]; j++){            
            for(int k=j; k>0; k--){
                if(player[i][k-1].rating < player[i][k].rating){
                    Player temp = player[i][k-1];
                    player[i][k-1] = player[i][k];
                    player[i][k] = temp;
                }
                else{
                    break;
                }
            }        
        }
    }
}
```
---
- Function buatTim(int bek, int gelandang, int striker) dimana dari kiri ke kanan merupakan jumlah bek, gelandang, dan penyerang
- Menggunakan fprintf untuk menuliskan nama file png ke file txt berdasarkan rating tertinggi untuk membentuk 11 pemain dimana kiper hanya 1.
- Setelah menulis file png kesebelas pemain maka file txt dipindah ke /home/[user]/ dengan command mv
```
void buatTim(int bek, int gelandang, int striker){
    sort();
    char filename[30];
    sprintf(filename, "Formasi_%d-%d-%d.txt", bek, gelandang, striker);
    FILE *filetxt = fopen(filename, "a+");
    if(filetxt == NULL){
        printf("Failed to open the file\n");
        return;
    }
    for(int i=0; i<bek; i++){
        fprintf(filetxt, "%s\n", player[0][i].fl_name);
    }
    for(int i=0; i<gelandang; i++){
        fprintf(filetxt, "%s\n", player[1][i].fl_name);
    }
    for(int i=0; i<striker; i++){
        fprintf(filetxt, "%s\n", player[2][i].fl_name);
    }
    fprintf(filetxt, "%s\n", player[3][0].fl_name);
    char *argv[] = {"mv", filename, "/home/vboxuser/", NULL};
    exec(argv[0], argv);
}
```
---

## Soal 4
Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.

Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:
Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.
Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.
Bonus poin apabila CPU state minimum.
Contoh untuk run: /program \* 44 5 /home/Banabil/programcron.sh

Pertama, kita mencek waktu sekarang dengan menggunakan

```
time_t waktu;
    struct tm *waktu_now;
    
    time(&waktu);
    waktu_now = localtime(&waktu);
    
    int jam_now = waktu_now->tm_hour;
    int mnt_now = waktu_now->tm_min;
    int dtk_now = waktu_now->tm_sec;
```
Berikutnya adalah mencek argumen, dimana argumen yang valid jika argc != 5. Selain itu, kita juga perlu mencek apakah jam, menit, atau detik yang dimasukkan valid atau tidak

```
if (argc != 5) {
        printf("argumen tidak valid\n");
        return 0;
    }

    if (argv[1] == "*") cekj = 0;
    else{
        cekj = 1;
        jam = atoi(argv[1]);
        if (0 > jam || jam > 23) printf("jam tidak valid\n");
    }

    if (argv[2] == "*") cekm = 0;
    else{
        cekm = 1;
        mnt = atoi(argv[2]);
        if (0 > mnt || mnt > 59) printf("menit tidak valid\n");
    }

    if (argv[3] == "*") cekd = 0;
    else{
        cekd = 1;
        dtk = atoi(argv[3]);
        if (0 > dtk || dtk > 59) printf("detik tidak valid\n");
    }
    char *path = argv[4];
    char *args[] = {"/bin/bash", path, NULL};
```

Lalu, karena cekj, cekm, dan cekd masing-masing memiliki 2 kemungkinan, yaitu 0 untuk * dan 1 untuk bilangannya, maka akan ada 8 kemungkinan case nantinya

Jika cekj = 1, maka kita akan mencek apakah jam yang dituju saat ini sudah berlalu, belum berlalu, atau sedang berjalan

```
//case jam pada hari ini sudah lewat
if (jam_now > jam){
    next_in = 86400 - ((jam_now - jam) * 3600 - (mnt_now - mnt) * 60 - (dtk_now - dtk));
    sleep(next_in);
}
//case jam pada hari ini belum lewat
if (jam_now < jam){
    next_in = (jam - jam_now) * 3600 - (mnt_now - mnt) * 60 - (dtk_now - dtk);
    sleep(next_in);
}
//case jam pada hari ini sedang berjalan
if (jam_now == jam){
    //cek menit
}
```

Jika cekm = 1, maka kita akan mencek apakah menit yang dituju saat ini sudah berlalu, belum berlalu, atau sedang berjalan

```
//case menit pada jam saat ini sudah lewat
if (mnt_now > mnt){
    next_in = 3600 - ((mnt_now - mnt) * 60 - (dtk_now - dtk));
    sleep(next_in);
}
//case menit pada jam saat ini belum lewat
if (mnt_now < mnt){
    next_in = (mnt_now - mnt) * 60 - (dtk_now - dtk);
    sleep(next_in);
}
//case menit pada jam saat ini sedang berjalan
if (mnt_now == mnt){
    //cek detik
}
```

Jika cekd = 1, maka kita akan mencek apakah detik yang dituju sudah berlalu atalu belum berlalu

```
//case detik pada menit saat ini sudah lewat
if (dtk_now >= dtk){
    next_in = 60 - (dtk_now - dtk);
    sleep(next_in);
}
//case detik pada menit saat ini belum lewat
if (dtk_now < dtk){
    next_in = dtk - dtk_now;
    sleep(next_in);
}
```

Pada pengecekan waktu di atas, dibuat sebuah variabel bernama repeat yang akan menyimpan berapa lama waktu tunggu hingga giliran berikutnya. Jika cekj = 1, maka repeat = 86400 (1 hari). Jika cekj = 0 namun cekm = 1, maka repeat = 3600. Dan jika cekj dan cekm sama dengan 0, sementara cekd = 1, maka repeat = 60.
