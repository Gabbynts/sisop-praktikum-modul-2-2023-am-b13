#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

int count[4] = {0};

typedef struct players_data{
    int rating;
    char fl_name[100];
} Player;

Player player[4][40];

void exec(char path[], char *argv[]){
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    execvp(path, argv);
  } 
  else{
    while ((wait(&status)) > 0);
  }
}

void download(char address[], char file[]){
    char *argv[] = {"wget", address, "-O", file, "-q", NULL};
    exec(argv[0], argv);
}

void unzip_delete(char file[]){
    char *arg[] = {"unzip", "-q", file, NULL};
    exec(arg[0], arg);
    char *argv[] = {"rm", file, NULL};
    exec(argv[0], argv);
}

void rmv(char dir[]){
    DIR *dp;
    struct dirent *ep;
    dp = opendir(dir);
    char *token;
    char file[100];
    if(dir == NULL){
        printf("Failed to open directory");
        return;
    }
    while((ep = readdir(dp)) != NULL) {
        if(ep->d_type == DT_REG){
            char *token = ep->d_name;
            char *separator = strchr(token, '_');
            if(separator != NULL){
                if(strstr(token, "ManUtd")){
                    continue;
                }
                else{
                    sprintf(file, "%s%s", dir, token);
                    char *arg[] = {"rm", file, NULL};
                    exec(arg[0], arg);
                }
            }
        }
    }    
}

void makeDirectory(char path[]){   
    pid_t pid;
    int status;
    for(int i=0; i<4; i++){
        pid = fork();
        char dir[100];
        if(pid < 0){
            exit(EXIT_FAILURE);
        }
        else if(pid == 0){
            if(i == 0){
            	sprintf(dir, "%sKiper", path);
                char *arga[] = {"mkdir", "-p", dir, NULL};
                execvp(arga[0], arga);
            }
            else if(i == 1){
                sprintf(dir, "%sBek", path);
                char *argb[] = {"mkdir", "-p", dir, NULL};
                execvp(argb[0], argb);
            }
            else if(i == 2){
                sprintf(dir, "%sGelandang", path);
                char *argc[] = {"mkdir", "-p", dir, NULL};
                execvp(argc[0], argc);
            }
            else if(i == 3){
                sprintf(dir, "%sPenyerang", path);
                char *argd[] = {"mkdir", "-p", dir, NULL};
                execvp(argd[0], argd);
            }
        }
    }
    while ((pid = wait(&status)) > 0);
}

void filter(char path[]){
    DIR *dp;
    struct dirent *ep;    
    dp = opendir(path);
    if (dp != NULL){
        pid_t child_id;
        int status;
        while(ep = readdir(dp)){   
            char source[100];
            char target[100];
            char *filename = ep->d_name;
            char *token;    
            Player data_p;        
            if (ep->d_type == DT_REG){
                child_id = fork();
                if(child_id < 0){
                	exit(EXIT_FAILURE);
            	}	
            	else if(child_id == 0){                
                    if(strstr(filename, "Bek") != NULL) {            
                        sprintf(source, "%s%s", path, filename);
                	    sprintf(target, "%sBek/", path); 
                        char *arg[] = {"mv", source, target, NULL};
                        execvp(arg[0], arg);
                        strcpy(data_p.fl_name, filename);
                        token = strtok(filename, "_");
                        token = strtok(NULL, "_");
                        token = strtok(NULL, "_");
                        token = strtok(NULL, "_");
                        data_p.rating = atoi(token);
                        player[0][count[0]] = data_p;
                        count[0]++;
                    }
                    else if(strstr(filename, "Gelandang") != NULL) {
                        sprintf(source, "%s%s", path, filename);
                	    sprintf(target, "%sGelandang/", path); 
                        char *arg[] = {"mv", source, target, NULL};
                        execvp(arg[0], arg);
                        strcpy(data_p.fl_name, filename);
                        token = strtok(filename, "_");
                        token = strtok(NULL, "_");
                        token = strtok(NULL, "_");
                        token = strtok(NULL, "_");
                        data_p.rating = atoi(token);
                        player[1][count[1]] = data_p;
                        count[1]++;
                    }
                    else if(strstr(filename, "Penyerang") != NULL) {
                        sprintf(source, "%s%s", path, filename);
                	    sprintf(target, "%sPenyerang/", path); 
                        char *arg[] = {"mv", source, target, NULL};
                        execvp(arg[0], arg);
                        strcpy(data_p.fl_name, filename);
                        token = strtok(filename, "_");
                        token = strtok(NULL, "_");
                        token = strtok(NULL, "_");
                        token = strtok(NULL, "_");
                        data_p.rating = atoi(token);
                        player[2][count[2]] = data_p;
                        count[2]++;
                    }
                    else if(strstr(filename, "Kiper") != NULL) {     
                        sprintf(source, "%s%s", path, filename);
                	    sprintf(target, "%sKiper/", path); 
                        char *arg[] = {"mv", source, target, NULL};
                        execvp(arg[0], arg);
                        strcpy(data_p.fl_name, filename);
                        token = strtok(filename, "_");
                        token = strtok(NULL, "_");
                        token = strtok(NULL, "_");
                        token = strtok(NULL, "_");
                        data_p.rating = atoi(token);
                        player[3][count[3]] = data_p;
                        count[3]++;
                    }
                }
            }
        }                   
            (void) closedir (dp);
            while ((child_id = wait(&status)) > 0);
    }
    else{
        perror ("Couldn't open the directory");
    }
}

void sort(){
    for(int i=0; i<4; i++){
        for(int j=1; j<count[i]; j++){            
            for(int k=j; k>0; k--){
                if(player[i][k-1].rating < player[i][k].rating){
                    Player temp = player[i][k-1];
                    player[i][k-1] = player[i][k];
                    player[i][k] = temp;
                }
                else{
                    break;
                }
            }        
        }
    }
}

void buatTim(int bek, int gelandang, int striker){
    sort();
    char filename[30];
    sprintf(filename, "Formasi_%d-%d-%d.txt", bek, gelandang, striker);
    FILE *filetxt = fopen(filename, "a+");
    if(filetxt == NULL){
        printf("Failed to open the file\n");
        return;
    }
    for(int i=0; i<bek; i++){
        fprintf(filetxt, "%s\n", player[0][i].fl_name);
    }
    for(int i=0; i<gelandang; i++){
        fprintf(filetxt, "%s\n", player[1][i].fl_name);
    }
    for(int i=0; i<striker; i++){
        fprintf(filetxt, "%s\n", player[2][i].fl_name);
    }
    fprintf(filetxt, "%s\n", player[3][0].fl_name);
    char *argv[] = {"mv", filename, "/home/vboxuser/", NULL};
    exec(argv[0], argv);
}

int main(){
    download("https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", "players.zip");
    unzip_delete("players.zip");
    rmv("./players/");
    makeDirectory("./players/");
    filter("./players/");
    buatTim(3, 4, 3);
    return 0;
}
