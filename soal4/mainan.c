#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>

int main(int argc, char *argv[]) {
    
    //cek waktu sekarang
    time_t waktu;
    struct tm *waktu_now;
    
    time(&waktu);
    waktu_now = localtime(&waktu);
    
    int jam_now = waktu_now->tm_hour;
    int mnt_now = waktu_now->tm_min;
    int dtk_now = waktu_now->tm_sec;
    
    int jam, mnt, dtk, cekj, cekm, cekd;
    
    //cek argumen
    if (argc != 5) {
        printf("argumen tidak valid\n");
        return 0;
    }

    if (argv[1] == "*") cekj = 0;
    else{
        cekj = 1;
        jam = atoi(argv[1]);
        if (0 > jam || jam > 23) printf("jam tidak valid\n");
    }

    if (argv[2] == "*") cekm = 0;
    else{
        cekm = 1;
        mnt = atoi(argv[2]);
        if (0 > mnt || mnt > 59) printf("menit tidak valid\n");
    }

    if (argv[3] == "*") cekd = 0;
    else{
        cekd = 1;
        dtk = atoi(argv[3]);
        if (0 > dtk || dtk > 59) printf("detik tidak valid\n");
    }
    char *path = argv[4];
    char *args[] = {"/bin/bash", path, NULL};

    //buat daemon
    pid_t pid, sid;

    pid = fork();
    
    if (pid < 0) exit(EXIT_FAILURE);
    if (pid > 0) exit(EXIT_SUCCESS);

    umask(0);

    sid = setsid();
    if (sid < 0) exit(EXIT_FAILURE);

    if ((chdir("/")) < 0) exit(EXIT_FAILURE);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    
    //cron-cron an
    //variabel untuk menyimpan lama sleep dalam detik yang diperlukan
    int next_in, repeat;
    //ada 8 kemungkinan
    
    //1. (jam menit detik)
    if (cekj == 1 && cekm == 1 && cekd == 1){
        //case jam pada hari ini sudah lewat
        if (jam_now > jam){
            next_in = 86400 - ((jam_now - jam) * 3600 - (mnt_now - mnt) * 60 - (dtk_now - dtk));
            sleep(next_in);
        }
        //case jam pada hari ini belum lewat
        if (jam_now < jam){
            next_in = (jam - jam_now) * 3600 - (mnt_now - mnt) * 60 - (dtk_now - dtk);
            sleep(next_in);
        }
        //case jam pada hari ini sedang berjalan
        if (jam_now == jam){
            //case menit pada jam saat ini sudah lewat
            if (mnt_now > mnt){
                next_in = 86400 - ((jam_now - jam) * 3600 - (mnt_now - mnt) * 60 - (dtk_now - dtk));
                sleep(next_in);
            }
            //case menit pada jam saat ini belum lewat
            if (mnt_now < mnt){
                next_in = (mnt_now - mnt) * 60 - (dtk_now - dtk);
                sleep(next_in);
            }
            //case menit pada jam saat ini sedang berjalan
            if (mnt_now == mnt){
                //case detik pada menit saat ini sudah lewat
                if (dtk_now >= dtk){
                    next_in = 86400 - ((jam_now - jam) * 3600 - (mnt_now - mnt) * 60 - (dtk_now - dtk));
                    sleep(next_in);
                }
                //case detik pada menit saat ini belum lewat
                if (dtk_now < dtk){
                    next_in = dtk - dtk_now;
                    sleep(next_in);
                }
            }
        }
        repeat = 86400;
    }
    
    //2. (jam menit *)
    if (cekj == 1 && cekm == 1 && cekd == 0){
        //case jam hari ini sudah lewat
        if (jam_now > jam){
            next_in = 86400 - ((jam_now - jam) * 3600 - (mnt_now - mnt) * 60 - (dtk_now));
            sleep(next_in);
        }
        //case jam hari ini belum lewat
        if (jam_now < jam){
            next_in = (jam - jam_now) * 3600 - (mnt_now - mnt) * 60 - (dtk_now);
            sleep(next_in);
        }
        //case jam hari ini sedang berjalan
        if (jam_now == jam){
            //case menit pada jam saat ini sudah lewat
            if (mnt_now >= mnt){
                next_in = 86400 - ((jam_now - jam) * 3600 - (mnt_now - mnt) * 60 - (dtk_now));
                sleep(next_in);
            }
            //case menit pada jam saat ini belum lewat
            if (mnt_now < mnt){
                next_in = (mnt_now - mnt) * 60 - (dtk_now);
                sleep(next_in);
            }
        }
        repeat = 86400;
    }
    
    //3. (jam * detik)
    if (cekj == 1 && cekm == 0 && cekd == 1){
        //case jam pada hari ini sudah lewat
        if (jam_now > jam){
            next_in = 86400 - ((jam_now - jam)*3600 - (mnt_now) * 60 - (dtk_now - dtk));
            sleep(next_in);
        }
        //case jam pada hari ini belum lewat
        if (jam_now < jam){
            next_in = (jam - jam_now) * 3600 - (mnt_now) * 60 - (dtk_now - dtk);
            sleep(next_in);
        }
        //case jam pada hari ini sedang berjalan
        if (jam_now == jam){
            //case detik pada menit saat ini sudah lewat
            if (dtk_now >= dtk){
                next_in = 86400 - ((jam_now - jam)*3600 - (mnt_now) * 60 - (dtk_now - dtk));
                sleep(next_in);
            }
            //case detik pada menit saat ini belum lewat
            if (dtk_now < dtk){
                next_in = dtk - dtk_now;
                sleep(next_in);
            }
        }
        repeat = 86400;
    }
    
    //4. (jam * *)
    if (cekj == 1 && cekm == 0 && cekd == 0){
        //case jam hari ini sudah lewat
        if (jam_now >= jam){
            next_in = 86400 - ((jam_now - jam) * 3600 - (mnt_now) * 60 - (dtk_now));
            sleep(next_in);
        }
        //case jam hari ini belum lewat
        if (jam_now < jam){
            next_in = (jam - jam_now) * 3600 - (mnt_now) * 60 - (dtk_now);
            sleep(next_in);
        }
        repeat = 86400;
    }
    
    //5. (* menit detik)
    if (cekj == 0 && cekm == 1 && cekd == 1){
        //case menit pada jam saat ini sudah lewat
        if (mnt_now > mnt){
            next_in = 3600 - ((mnt_now - mnt) * 60 - (dtk_now - dtk));
            sleep(next_in);
        }
        //case menit pada jam saat ini belum lewat
        if (mnt_now < mnt){
            next_in = (mnt_now - mnt) * 60 - (dtk_now - dtk);
            sleep(next_in);
        }
        //case menit pada jam saat ini sedang berjalan
        if (mnt_now == mnt){
            //case detik pada menit saat ini sudah lewat
            if (dtk_now >= dtk){
                next_in = 60 - (dtk_now - dtk);
                sleep(next_in);
            }
            //case detik pada menit saat ini belum lewat
            if (dtk_now < dtk){
                next_in = dtk - dtk_now;
                sleep(next_in);
            }
        }
        repeat = 3600;
    }
    
    //6. (* menit *)
    if (cekj == 0 && cekm == 1 && cekd == 0){
        //case menit pada jam saat ini sudah lewat
        if (mnt_now >= mnt){
            next_in = 3600 - ((mnt_now - mnt) * 60 - (dtk_now));
            sleep(next_in);
        }
        //case menit pada jam saat ini belum lewat
        if (mnt_now < mnt){
            next_in = (mnt_now - mnt) * 60 - (dtk_now);
            sleep(next_in);
        }
        repeat = 3600;
    }
    
    //7. (* * detik)
    if (cekj == 0 && cekm == 0 && cekd == 1){
        //case detik pada menit saat ini sudah lewat
        if (dtk_now >= dtk){
            next_in = 60 - (dtk_now - dtk);
            sleep(next_in);
        }
        //case detik pada menit saat ini belum lewat
        if (dtk_now < dtk){
            next_in = dtk - dtk_now;
            sleep(next_in);
        }
        repeat = 60;
    }

    //8. (* * *)
    if (cekj == 0 && cekm == 0 && cekd == 0){
        repeat = 1;
    }
    
    while (1) {
        pid_t pidlagi;
        pidlagi = fork();  
        if (pidlagi < 0) exit(EXIT_FAILURE);
        if (pidlagi == 0) execvp(args[0], args);
        sleep(repeat);
    }

    return 0;
}