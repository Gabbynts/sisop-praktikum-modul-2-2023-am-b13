#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/wait.h>
#include <unistd.h>

#define DOWNLOAD_DELAY 5
#define DIRECTORY_DELAY 30

void makeDirectory(char *argv[], pid_t pid);
void download(char* directory);
void zip(char* directory);
void killer(char **argv, pid_t pid);

void makeDirectory(char *argv[], pid_t pid) {
  time_t epoch;
  struct tm* dateLocal;
  
  char directory[100];

  while (1) {
    epoch = time(NULL);
    dateLocal = localtime(&epoch);

    strftime(directory, 100, "%Y-%m-%d_%H:%M:%S", dateLocal);

    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
      exit(EXIT_FAILURE);
    }

    if (child_id == 0) {

      if (fork() == 0) {

        char *mkdir_argv[] = {"mkdir", "-p", directory, NULL};
        execvp("mkdir", mkdir_argv);

      } 
      else {
        while (wait(&status) > 0);

        download(directory);
        zip(directory);
      }
    } 
    else {
      sleep(DIRECTORY_DELAY);    
    }
  }
}

void download(char* directory) {
  chdir(directory);

  for (int i = 0; i < 15; i++) {
    pid_t child_id;
    child_id = fork();

    if (child_id == 0) {
      time_t epoch; 
      struct tm* dateLocal;

      epoch = time(NULL);
      dateLocal = localtime(&epoch);

      char url[100];
      char filename[100];
      time_t t = time(NULL);

      sprintf(url, "https://picsum.photos/%ld", (t % 1000) + 50);
      
      strftime(filename, 100, "%Y-%m-%d_%H:%M:%S", dateLocal);

      char *argv[] = {"wget", url, "-qO", filename, NULL};
      execvp("wget", argv);
    }
    sleep(DOWNLOAD_DELAY);
  }
}

void zip(char* directory) {
  chdir("..");

  char filename[100];
  sprintf(filename, "%.90s.zip", directory);

  char *argv[] = {"zip", "-qrm", filename, directory, NULL};
  execvp("zip", argv);
}

void killer(char **argv, pid_t pid) {
  char *mode = argv[1]; 
  
  FILE *filename = fopen("killer.c", "w");

  fprintf(filename, "#include <stdlib.h>\n");
  fprintf(filename, "#include <string.h>\n");
  fprintf(filename, "#include <unistd.h>\n");
  fprintf(filename, "#include <stdio.h>\n");
  fprintf(filename, "#include <signal.h>\n\n");
  fprintf(filename, "int main(int argc, char *argv[]) {\n");
  fprintf(filename, "  pid_t pid = %d;\n", pid);
  
  if (strcmp(mode, "-a") == 0) {
    fprintf(filename, "  kill(-pid, SIGKILL);\n");
  } 

  else if (strcmp(mode, "-b") == 0) {
    fprintf(filename, "  kill(pid, SIGTERM);\n");
  }
  
  fprintf(filename, "  remove(\"killer.c\");\n");
  fprintf(filename, "  remove(\"killer.o\");\n");
  fprintf(filename, "  return 0;\n");
  fprintf(filename, "}\n");

  fclose(filename);

  char *arg[] = {"gcc", "killer.c", "-o", "killer.o", NULL};
  execvp("gcc", arg);
}

int main(int argc, char **argv) {

  pid_t pid, sid;
  pid = fork();

  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid > 0) {
    if (strcmp(argv[1], "-a") == 0 || strcmp(argv[1], "-b") == 0) {
      killer(argv, pid);
    } 
    else {
      exit(EXIT_SUCCESS);
    }
  }

  umask(0);

  sid = setsid();

  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  makeDirectory(argv, getpid());

  return EXIT_SUCCESS;
}
